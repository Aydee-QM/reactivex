import { of, from } from 'rxjs';

/**
 * of = toma argumentos y genera una secuencia
 * from = array, promesa,iterable, observable
 */

const observer = {
  next: val => console.log('next:', val),
  complete: () => console.log('complete')
};

const myGenerator  = function*(){ // función generadora
  yield 1;
  yield 2;
  yield 3;
  yield 4;
  yield 5;
}

const miIterable = myGenerator();

/* for( let id of miIterable ) {
  console.log(id);
} */

from( miIterable ).subscribe(observer)



// const source$ = from([1, 2, 3, 4, 5]);
// const source$ = of(...[1, 2, 3, 4, 5]);
// const source$ = from('Aydee');

const source$ = from(fetch('https://api.github.com/users/klerith'));

// source$.subscribe(observer);

/* source$.subscribe( async response => {
  console.log(response)
  const data = await response.json();
  console.log(data)
}); */
