import { fromEvent } from 'rxjs';
import { map, tap } from 'rxjs/operators';

const text = document.createElement('div');
text.innerHTML = `
  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent mattis turpis risus, id aliquam dolor aliquet sed. Suspendisse potenti. Curabitur hendrerit auctor orci, imperdiet efficitur nisl rhoncus in. Fusce hendrerit turpis in dapibus dignissim. Ut elementum dui orci, sit amet aliquam tellus tristique vitae. Vivamus ante lectus, tempor sed enim consectetur, mattis condimentum augue. Suspendisse sapien enim, dapibus id justo ut, sodales molestie diam.
  <br><br/>
  Vestibulum egestas in orci in tempor. Cras in molestie mauris. Cras non efficitur quam. Morbi faucibus interdum libero. Suspendisse accumsan aliquet mattis. Proin enim lorem, porta nec pharetra sed, consectetur nec ipsum. In a pharetra tortor. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla id pulvinar nisi. Curabitur consectetur ultrices neque, eget tincidunt ligula rhoncus quis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla nec dignissim metus. Nam nec quam a lorem auctor eleifend. Sed eget feugiat velit. Morbi pretium dignissim ante ut ullamcorper.
  <br><br/>
  Curabitur iaculis quis ex id sagittis. Mauris eget orci tellus. Phasellus augue libero, vestibulum nec tincidunt vitae, pulvinar sed massa. Proin ac sapien pellentesque ipsum porta mattis. Ut convallis libero ligula, posuere dignissim eros hendrerit sed. Morbi egestas at turpis vel sollicitudin. Fusce et lorem nec enim vestibulum accumsan. Nullam diam neque, iaculis in libero a, condimentum tempus mauris. Maecenas fringilla est a facilisis suscipit. Quisque justo ex, vestibulum a turpis vitae, vestibulum ultrices metus. Proin mattis posuere mi sit amet sagittis. Integer nec dolor ac nunc auctor tempor. In vehicula commodo ante, quis consectetur enim venenatis sed. Ut sed nunc a ligula interdum fringilla.
  <br><br/>
  Nam varius, nunc ac molestie efficitur, felis neque pellentesque neque, in auctor tortor leo tincidunt nisl. Suspendisse eu leo quis lacus tempus placerat. Maecenas rhoncus magna a metus ornare, at lacinia metus dictum. Suspendisse quam sapien, aliquam et dictum vel, rhoncus id leo. Phasellus metus risus, suscipit id mattis non, ultricies a magna. Integer lacinia iaculis ligula, vel aliquet nunc volutpat a. Quisque accumsan dictum nisi. Ut nec dignissim urna, vitae blandit nunc. Curabitur vulputate lorem vel porttitor viverra. Proin et efficitur justo. Nulla at gravida sem, id tempor erat. Curabitur justo leo, pellentesque ut pharetra vitae, congue tristique nulla. Sed dignissim turpis maximus ipsum interdum, ac fringilla dolor sagittis. Mauris ac porta felis. Vivamus quis nibh vitae ante mattis lobortis. Duis accumsan risus ligula, et commodo est fringilla eget.
  <br><br/>
  Fusce pulvinar, libero eu aliquet sollicitudin, magna turpis sodales magna, a volutpat nibh metus vel sapien. Vivamus vehicula iaculis felis, quis sollicitudin augue condimentum nec. Quisque in condimentum felis, eu lobortis quam. Nunc dignissim nibh vel luctus auctor. Integer ultricies lectus nulla, ac suscipit ipsum vestibulum ut. Maecenas vel justo nec enim auctor semper. Sed mollis convallis purus, ac porttitor nunc varius finibus. Etiam laoreet congue enim et tempor. Suspendisse suscipit suscipit gravida. Suspendisse tempor ipsum mauris, et porta sapien laoreet nec. Ut egestas, mauris feugiat laoreet facilisis, tortor nunc blandit nunc, vitae consequat sapien nisl id enim. Aenean volutpat sapien in accumsan vulputate.
`

const body = document.querySelector('body');
body.append(text);

const progressBar = document.createElement('div');
progressBar.setAttribute('class', 'progress-bar');
body.append(progressBar)

// function to calculate
const calcPorcentScroll = (event) => {
  const {
    scrollTop,
    scrollHeight,
    clientHeight
  } = event.target.documentElement;

  return (scrollTop / (scrollHeight - clientHeight) ) * 100;
} 

// Streams
const scroll$ = fromEvent(document, 'scroll');
//scroll$.subscribe(console.log)

const progress$ = scroll$.pipe(
  // map(event => calcPorcentScroll(event))
  map(calcPorcentScroll),
  tap(console.log)
);

progress$.subscribe( porcent => {
  progressBar.style.width = `${porcent}%`;
});