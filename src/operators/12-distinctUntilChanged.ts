import { of, from } from 'rxjs';
import { distinct, distinctUntilChanged } from 'rxjs/operators';

const numbers$ = of<number|string>(1,'1',1,3,3,2,2,4,4,5,3,1, '1');

numbers$.pipe(
  distinctUntilChanged() // ===
)
  .subscribe(console.log);

interface Personaje {
  name: string;
}

const personajes: Personaje[] = [
  {
    name: 'Megaman'
  },
  {
    name: 'X'
  },
  {
    name: 'Megaman'
  },
  {
    name: 'Dr. Willy'
  },
  {
    name: 'X'
  },
  {
    name: 'X'
  },
  {
    name: 'Zero'
  }
];

from( personajes ).pipe(
  distinctUntilChanged((prev, now) => prev.name === now.name)
)
  .subscribe(console.log)